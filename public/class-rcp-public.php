<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://realtyna.com/
 * @since      1.0.0
 *
 * @package    Rcp
 * @subpackage Rcp/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Rcp
 * @subpackage Rcp/public
 * @author     Noah s <noah.s@realtyna.com>
 */
class Rcp_Public {

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $rcp    The ID of this plugin.
     */
    private $rcp;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $rcp       The name of the plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct( $rcp, $version ) {

        $this->rcp = $rcp;
        $this->version = $version;

        $this->rewriteRules();
    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_styles() {

        /**
         * An instance of this class should be passed to the run() function
         * defined in Rcp_Loader as all the hooks are defined
         * in that particular class.
         *
         * The Rcp_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        $rcpFrontPage = intval( get_query_var( 'rcpvar' ) );
        if ( $rcpFrontPage ) {
            wp_enqueue_style('bootstrap', RCP_URL . 'public/assets/css/bootstrap.min.css', array(), '4.3.1', 'all');
            wp_enqueue_style($this->rcp, RCP_URL . 'public/assets/css/rcp-public.css', array('bootstrap'), $this->version, 'all');
        }
    }

    /**
     * Register the JavaScript for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts() {

        /**
         * An instance of this class should be passed to the run() function
         * defined in Rcp_Loader as all the hooks are defined
         * in that particular class.
         *
         * The Rcp_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        $rcpFrontPage = intval( get_query_var( 'rcpvar' ) );
        if ( $rcpFrontPage ) {
            wp_enqueue_script( 'bootstrap', RCP_URL . 'public/assets/js/bootstrap.min.js', array( 'jquery' ), '4.3.1', false );
            wp_enqueue_script( $this->rcp, RCP_URL . 'public/assets/js/rcp-public.js', array( 'jquery', 'bootstrap' ), $this->version, false );
        }
    }

    /**
     * Perform necessary rewrite rules for public-facing of plugin
     *
     * @since    1.0.0
     */
    public static function rewriteRules() {
        add_filter( 'generate_rewrite_rules', function ( $wp_rewrite ){
            $wp_rewrite->rules = array_merge(
                ['rcp/?$' => 'index.php?rcpvar=1'],
                $wp_rewrite->rules
            );
        } );
        add_filter( 'query_vars', function( $query_vars ){
            $query_vars[] = 'rcpvar';
            return $query_vars;
        } );
        add_action( 'template_redirect', function(){
            $rcpFrontPage = intval( get_query_var( 'rcpvar' ) );
            if ( $rcpFrontPage ) {
                include RCP_PATH . 'public/page-rcp.php';
                die;
            }
        } );
    }

}