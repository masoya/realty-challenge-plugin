<?php
$rcpPage = get_page_by_path('rcp');

if (!$rcpPage) {
    global $wp_query;
    $wp_query->set_404();
    status_header(404);
    get_template_part(404);
    exit();
}

$args = array(
    'post_type' => 'rcp_movie',
    'order' => 'ASC'
);

$theQuery = new WP_Query($args);
?>
<!doctype html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $rcpPage->post_title; ?></title>
    <meta name="author" content="SitePoint">

    <meta property="og:title" content="<?php echo $rcpPage->post_title; ?>">
    <meta property="og:type" content="website">
    <meta property="og:description" content="<?php echo $rcpPage->post_title; ?>">
    <meta property="og:image" content="image.png">

    <?php wp_head(); ?>
</head>

<body>

<div class="container-fluid rcp-container">

    <div class="p-5 text-center bg-light" style="margin-top: 58px;">
        <h1 class="mb-3"><?php echo $rcpPage->post_title; ?></h1>
        <div class="mb-3">
            <?php echo do_shortcode( $rcpPage->post_content ); ?>
        </div>
        <a type="button" class="btn btn-primary" href="<?php echo get_site_url() ?>/wp-json/wp/v2/rcp_movie" target="_blank">Fetch From REST API</a>
    </div>

    <div class="seperated">
        <h2 class="text-center text-info p-3 bg-dark mt-5">Seperated Movies List</h2>

        <?php if ($theQuery->have_posts()) : ?>
            <div class="row mb-2">
                <?php
                while ($theQuery->have_posts()) :
                    $theQuery->the_post();
                    $postId = get_the_ID();
                    $duration = get_post_meta($postId, 'movie_duration', true);
                    $imdbRating = get_post_meta($postId, 'movie_imdb_rating', true);
                    $director = get_post_meta($postId, 'movie_director', true);
                    $releaseDate = get_post_meta($postId, 'movie_release_date', true);

                    $image = get_the_post_thumbnail_url();

                    $genreTerms = get_the_terms( $postId, 'rcp_genre' );
                    $genres = join(', ', wp_list_pluck( $genreTerms , 'name') );

                    $castTerms = get_the_terms( $postId, 'rcp_cast' );
                    $cast = join(', ', wp_list_pluck( $castTerms , 'name') );
                    ?>
                    <div class="col-md-3 col-sm-6">
                        <div class="card">
                            <img src="<?php echo $image; ?>" class="card-img-top" alt="<?php the_title(); ?>">
                            <div class="card-body">
                                <h5 class="card-title"><?php the_title(); ?></h5>
                                <div class="card-text"><?php the_content(); ?></div>
                                <div>
                                    <small class="text-muted">Director: <span class="text-success"><?php echo $director ?></span></small>
                                </div>
                                <div>
                                    <small class="text-muted">ReleaseDate: <span class="text-success"><?php echo $releaseDate ?></span></small>
                                </div>
                                <div>
                                    <small class="text-muted">Genre: <span class="text-danger"><?php echo $genres ?></span></small>
                                </div>
                                <div>
                                    <small class="text-muted">Cast: <span class="text-info"><?php echo $cast ?></span></small>
                                </div>
                            </div>
                            <div class="card-footer d-flex justify-content-between">
                                <div>
                                    <small class="text-muted">Duration: <span class="text-info"><?php echo $duration ?></span></small>
                                </div>
                                <div>
                                    <small class="text-muted">IMDB: <span class="text-danger"><?php echo $imdbRating ?></span></small>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                endwhile;
                wp_reset_postdata();
                ?>
            </div>
        <?php else: ?>
            <div class="p-5 text-center bg-danger text-white mb-2">
                There Is No Movies Yet!!
            </div>
        <?php endif; ?>
    </div>
</div>

<?php wp_footer(); ?>
</body>
</html>