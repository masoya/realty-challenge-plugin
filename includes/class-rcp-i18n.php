<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://realtyna.com/
 * @since      1.0.0
 *
 * @package    Rcp
 * @subpackage Rcp/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Rcp
 * @subpackage Rcp/includes
 * @author     Noah s <noah.s@realtyna.com>
 */
class Rcp_i18n {


    /**
     * Load the plugin text domain for translation.
     *
     * @since    1.0.0
     */
    public function loadPluginTextDomain() {
        load_plugin_textdomain(
            'realty_challenge',
            false,
            RCP_PATH . '/languages/'
        );

    }



}