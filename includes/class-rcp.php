<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://realtyna.com/
 * @since      1.0.0
 *
 * @package    Rcp
 * @subpackage Rcp/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Rcp
 * @subpackage Rcp/includes
 * @author     Noah s <noah.s@realtyna.com>
 */
class Rcp {

    /**
     * The loader that's responsible for maintaining and registering all hooks that power
     * the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      Rcp_Loader    $loader    Maintains and registers all hooks for the plugin.
     */
    protected $loader;

    /**
     * The unique identifier of this plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string    $rcp    The string used to uniquely identify this plugin.
     */
    protected $rcp;

    /**
     * The current version of the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string    $version    The current version of the plugin.
     */
    protected $version;

    /**
     * Define the core functionality of the plugin.
     *
     * Set the plugin name and the plugin version that can be used throughout the plugin.
     * Load the dependencies, define the locale, and set the hooks for the admin area and
     * the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function __construct() {
        if ( defined( 'RCP_VERSION' ) ) {
            $this->version = RCP_VERSION;
        } else {
            $this->version = '1.0.0';
        }
        $this->rcp = 'rcp';

        $this->loadDependencies();
        $this->setLocale();
        $this->defineAdminHooks();
        $this->definePublicHooks();

    }

    /**
     * Load the required dependencies for this plugin.
     *
     * Include the following files that make up the plugin:
     *
     * - Rcp_Loader. Orchestrates the hooks of the plugin.
     * - Rcp_i18n. Defines internationalization functionality.
     * - Rcp_Admin. Defines all hooks for the admin area.
     * - Rcp_Public. Defines all hooks for the public side of the site.
     *
     * Create an instance of the loader which will be used to register the hooks
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */
    private function loadDependencies() {

        /**
         * The class responsible for orchestrating the actions and filters of the
         * core plugin.
         */
        require_once RCP_PATH . 'includes/class-rcp-loader.php';

        /**
         * The class responsible for defining internationalization functionality
         * of the plugin.
         */
        require_once RCP_PATH . 'includes/class-rcp-i18n.php';

        /**
         * The class responsible for defining all actions that occur in the admin area.
         */
        require_once RCP_PATH . 'admin/class-rcp-admin.php';

        /**
         * The class responsible for defining all actions that occur in the public-facing
         * side of the site.
         */
        require_once RCP_PATH . 'public/class-rcp-public.php';

        $this->loader = new Rcp_Loader();

    }

    /**
     * Define the locale for this plugin for internationalization.
     *
     * Uses the Rcp_i18n class in order to set the domain and to register the hook
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */
    private function setLocale() {

        $pluginI18n = new Rcp_i18n();

        $this->loader->addAction( 'plugins_loaded', $pluginI18n, 'loadPluginTextDomain' );

    }

    /**
     * Register all the hooks related to the admin area functionality
     * of the plugin.
     *
     * @since    1.0.0
     * @access   private
     */
    private function defineAdminHooks() {

        $pluginAdmin = new Rcp_Admin( $this->getRcp(), $this->getVersion() );

        $this->loader->addAction( 'admin_enqueue_scripts', $pluginAdmin, 'enqueueStyles' );
        $this->loader->addAction( 'admin_enqueue_scripts', $pluginAdmin, 'enqueueScripts' );

        $this->loader->addAction( 'init', $pluginAdmin->moviePostType, 'register' );
        $this->loader->addAction( 'init', $pluginAdmin->moviePostType, 'registerTaxonomies', 0 );
        $this->loader->addAction( 'add_meta_boxes', $pluginAdmin->moviePostType, 'registerCustomFields' );
        $this->loader->addAction( 'save_post_rcp_movie', $pluginAdmin->moviePostType, 'saveFields', 10, 2 );
        $this->loader->addAction( 'widgets_init', $pluginAdmin, 'registerWidgets', 10, 2 );
    }

    /**
     * Register all the hooks related to the public-facing functionality
     * of the plugin.
     *
     * @since    1.0.0
     * @access   private
     */
    private function definePublicHooks() {

        $pluginPublic = new Rcp_Public( $this->getRcp(), $this->getVersion() );

        $this->loader->addAction( 'wp_enqueue_scripts', $pluginPublic, 'enqueue_styles' );
        $this->loader->addAction( 'wp_enqueue_scripts', $pluginPublic, 'enqueue_scripts' );
    }

    /**
     * Run the loader to execute all the hooks with WordPress.
     *
     * @since    1.0.0
     */
    public function run() {
        $this->loader->run();
    }

    /**
     * The name of the plugin used to uniquely identify it within the context of
     * WordPress and to define internationalization functionality.
     *
     * @since     1.0.0
     * @return    string    The name of the plugin.
     */
    public function getRcp() {
        return $this->rcp;
    }

    /**
     * The reference to the class that orchestrates the hooks with the plugin.
     *
     * @since     1.0.0
     * @return    Rcp_Loader    Orchestrates the hooks of the plugin.
     */
    public function getLoader() {
        return $this->loader;
    }

    /**
     * Retrieve the version number of the plugin.
     *
     * @since     1.0.0
     * @return    string    The version number of the plugin.
     */
    public function getVersion() {
        return $this->version;
    }

}