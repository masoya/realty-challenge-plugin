<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://realtyna.com/
 * @since      1.0.0
 *
 * @package    Rcp
 * @subpackage Rcp/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Rcp
 * @subpackage Rcp/includes
 * @author     Noah s <noah.s@realtyna.com>
 */
class Rcp_Uninstall {

    /**
     * @since    1.0.0
     */
    public function run() {
        $this->removePage();
    }

    /**
     * Remove Rcp plugin public page
     *
     * @since    1.0.0
     * @access   private
     */
    private function removePage () {
        $check_page_exist = get_page_by_path('rcp');
        // Check if the page not exists
        if(!empty($check_page_exist)) {
            $page_id = wp_delete_post($check_page_exist->ID);
        }
    }
}