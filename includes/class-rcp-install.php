<?php

/**
 * Fired during plugin activation
 *
 * @link       https://realtyna.com/
 * @since      1.0.0
 *
 * @package    Rcp
 * @subpackage Rcp/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Rcp
 * @subpackage Rcp/includes
 * @author     Noah s <noah.s@realtyna.com>
 */
class Rcp_Install {

    /**
     * @since    1.0.0
     */
    public function run() {
        // Register shortcodes that needed in cratePage function
        $shortcodesInstance = new Rcp_Shortcodes();
        $shortcodesInstance->register();

        // Create specific plugin public page for demonstration purposes
        $this->createPage();

        // Perform rewrite rules for public-facing of plugin
        Rcp_Public::rewriteRules();


        // Allows for automatic flushing of the WordPress rewrite rules
        flush_rewrite_rules(false);
    }

    /**
     * Create Rcp plugin public page
     *
     * @since    1.0.0
     * @access   private
     */
    private function createPage () {
        $check_page_exist = get_page_by_path('rcp');
        // Check if the page already exists
        if(empty($check_page_exist)) {
            $content = '<div>This is Realty Challenge Plugin (RCP) specific page for demonstration purposes only!<br>';
            $content .= 'This page will be deleted after plugin deactivation.<br><br></div>';
            $content .= '<div class="shortcode-section"><div><strong>Shortcode (rcp-movie-list):</strong></div>';
            $content .= '<div class="d-flex justify-content-center text-left">[rcp-movie-list]<br><br></div></div>';
            wp_insert_post(
                array(
                    'comment_status' => 'close',
                    'ping_status'    => 'close',
                    'post_author'    => 1,
                    'post_title'     => 'Realty Challenge Plugin (rcp)',
                    'post_name'      => 'rcp',
                    'post_status'    => 'publish',
                    'post_content'   => $content, //todo: shortcode must be added
                    'post_type'      => 'page'
                )
            );
        }
    }
}