<?php

/**
 * The movie post type setup of the plugin.
 *
 * @package    Rcp
 * @subpackage Rcp/admin
 * @author     Noah s <noah.s@realtyna.com>
 */
class Rcp_Movie_Post_Type {

    /**
     * Register movie post type for plugin
     *
     * @since    1.0.0
     */
    public function register() {
        register_post_type('rcp_movie',
            array(
                'labels' => array(
                    'name' => __('Movies', 'realtyna_challenge'),
                    'singular_name' => __('Movie', 'realtyna_challenge'),
                    'menu_name' => __('Movies', 'realtyna_challenge'),
                    'name_admin_bar' => __('Movie', 'realtyna_challenge'),
                    'add_new' => __('Add New', 'realtyna_challenge'),
                    'add_new_item' => __('Add New Movie', 'realtyna_challenge'),
                    'new_item' => __('New Movie', 'realtyna_challenge'),
                    'edit_item' => __('Edit Movie', 'realtyna_challenge'),
                    'view_item' => __('View Movie', 'realtyna_challenge'),
                    'all_items' => __('All Movies', 'realtyna_challenge'),
                    'search_items' => __('Search Movies', 'realtyna_challenge'),
                    'parent_item_colon' => __('Parent Movies:', 'realtyna_challenge'),
                    'not_found' => __('No movies found.', 'realtyna_challenge'),
                    'not_found_in_trash' => __('No movies found in Trash.', 'realtyna_challenge'),
                    'featured_image' => __('Movie Cover Image', 'realtyna_challenge'),
                    'set_featured_image' => __('Set cover image', 'realtyna_challenge'),
                    'remove_featured_image' => __('Remove cover image', 'realtyna_challenge'),
                    'use_featured_image' => __('Use as cover image', 'realtyna_challenge'),
                    'archives' => __('Movie archives', 'realtyna_challenge'),
                    'insert_into_item' => __('Insert into movie', 'realtyna_challenge'),
                    'uploaded_to_this_item' => __('Uploaded to this movie', 'realtyna_challenge'),
                    'filter_items_list' => __('Filter movies list', 'realtyna_challenge'),
                    'items_list_navigation' => __('Movies list navigation', 'realtyna_challenge'),
                    'items_list' => __('Movies list', 'realtyna_challenge'),
                ),
                'public' => true,
                'show_in_rest' => true,
                'supports' => array('title', 'editor', 'tags', 'thumbnail'),
                'has_archive' => true,
                'rewrite' => array('slug' => 'movies'),
                'menu_position' => 5,
                'menu_icon' => 'dashicons-format-video',
                'taxonomies' => array('genres')
            )
        );
    }

    /**
     * Register taxonomies for movie post type
     *
     * Register genre taxonomy for the movie post type and
     * register cast taxonomy (actors and actress) for the movie post type (tag-like)
     *
     * @since    1.0.0
     */
    public function registerTaxonomies() {
        register_taxonomy('rcp_genre', 'rcp_movie', array(
            'hierarchical' => true,
            'labels' => array(
                'name' => __('Genres', 'realtyna_challenge'),
                'singular_name' => __('Genre', 'realtyna_challenge'),
                'search_items' => __('Search Genres', 'realtyna_challenge'),
                'all_items' => __('All Genres', 'realtyna_challenge'),
                'parent_item' => __('Parent Genre', 'realtyna_challenge'),
                'parent_item_colon' => __('Parent Genre:', 'realtyna_challenge'),
                'edit_item' => __('Edit Genre', 'realtyna_challenge'),
                'update_item' => __('Update Genre', 'realtyna_challenge'),
                'add_new_item' => __('Add New Genre', 'realtyna_challenge'),
                'new_item_name' => __('New Genre Name', 'realtyna_challenge'),
                'menu_name' => __('Genres', 'realtyna_challenge'),
            ),
            'show_ui' => true,
            'show_in_rest' => true,
            'show_admin_column' => true,
        ));

        register_taxonomy('rcp_cast', 'rcp_movie', array(
            'hierarchical' => false,
            'labels' => array(
                'name' => __('Cast', 'realtyna_challenge'),
                'singular_name' => __('Cast', 'realtyna_challenge'),
                'search_items' => __('Search Cast', 'realtyna_challenge'),
                'all_items' => __('All Cast', 'realtyna_challenge'),
                'parent_item' => __('Parent Cast', 'realtyna_challenge'),
                'parent_item_colon' => __('Parent Cast:', 'realtyna_challenge'),
                'edit_item' => __('Edit Cast', 'realtyna_challenge'),
                'update_item' => __('Update Cast', 'realtyna_challenge'),
                'add_new_item' => __('Add New Cast', 'realtyna_challenge'),
                'new_item_name' => __('New Cast Name', 'realtyna_challenge'),
                'menu_name' => __('Cast', 'realtyna_challenge'),
            ),
            'show_ui' => true,
            'show_in_rest' => true,
            'show_admin_column' => true,
        ));
    }

    /**
     * Register custom fields for the movie post type
     *
     * @since    1.0.0
     */
    public function registerCustomFields() {
        add_meta_box('rcp_movie_extra_info', __('Extra Info', 'realtyna_challenge'), array($this, 'ExtraInfoHtmlOutput'), 'rcp_movie', 'normal', 'low');
    }

    /**
     * Extra info HTML output for movie post type custom fields
     *
     * @since    1.0.0
     */
    public function ExtraInfoHtmlOutput($post) {
        wp_nonce_field(basename(__FILE__), 'movie_fields_nonce'); //used later for security

        $imdbRating = get_post_meta($post->ID, 'movie_imdb_rating', true);
        $director = get_post_meta($post->ID, 'movie_director', true);
        $duration = get_post_meta($post->ID, 'movie_duration', true);
        $releaseDate = get_post_meta($post->ID, 'movie_release_date', true);
        ?>
        <div class="movie-field">
            <label for="movie_imdb_rating"><?php echo __('IMDB rating', 'realtyna_challenge'); ?></label>
            <div class="input">
                <input type="text" name="movie_imdb_rating" id="movie_imdb_rating" value="<?php echo $imdbRating; ?>" size="4"/>
            </div>
        </div>
        <div class="movie-field">
            <label for="movie_director"><?php echo __('Director', 'realtyna_challenge'); ?></label>
            <div class="input">
                <input type="text" name="movie_director" id="movie_director" value="<?php echo $director; ?>" size="40"/>
            </div>
        </div>
        <div class="movie-field">
            <label for="movie_duration"><?php echo __('Duration', 'realtyna_challenge'); ?></label>
            <div class="input">
                <input type="text" name="movie_duration" id="movie_duration" value="<?php echo $duration; ?>" size="7"/>
            </div>
        </div>
        <div class="movie-field">
            <label for="movie_release_date"><?php echo __('Release date', 'realtyna_challenge'); ?></label>
            <div class="input">
                <input type="text" name="movie_release_date" id="movie_release_date" value="<?php echo $releaseDate; ?>" size="50"/>
            </div>
        </div>
        <?php
    }

    /**
     * Save fields for movie post type
     *
     * @since    1.0.0
     */
    public function saveFields($post_id)
    {
        // check for nonce to top xss
        if (!isset($_POST['movie_fields_nonce']) || !wp_verify_nonce($_POST['movie_fields_nonce'], basename(__FILE__))) {
            return;
        }

        // check for correct user capabilities - stop internal xss from customers
        if (!current_user_can('edit_post', $post_id)) {
            return;
        }

        // update fields
        if (isset($_POST['movie_imdb_rating'])) {
            update_post_meta($post_id, 'movie_imdb_rating', sanitize_text_field($_POST['movie_imdb_rating']));
        }

        if (isset($_POST['movie_director'])) {
            update_post_meta($post_id, 'movie_director', sanitize_text_field($_POST['movie_director']));
        }

        if (isset($_POST['movie_duration'])) {
            update_post_meta($post_id, 'movie_duration', sanitize_text_field($_POST['movie_duration']));
        }

        if (isset($_POST['movie_release_date'])) {
            update_post_meta($post_id, 'movie_release_date', sanitize_text_field($_POST['movie_release_date']));
        }
    }
}