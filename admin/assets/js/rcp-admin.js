(function( $ ) {
    'use strict';

    /**
     * All the code for admin-facing JavaScript source
     * should reside in this file.
     *
     * The $ function reference has been prepared for usage within the scope
     * of this function.
     *
     * This enables you to define handlers, for when the DOM is ready:
     *
     * $(function() {
     *
     * });
     *
     * When the window is loaded:
     *
     * $( window ).load(function() {
     *
     * });
     *
     */

})( jQuery );