<?php

/**
 * The shortcodes setup of the plugin.
 *
 * @package    Rcp
 * @subpackage Rcp/admin
 * @author     Noah s <noah.s@realtyna.com>
 */
class Rcp_Shortcodes {

    /**
     * Initialize shortcodes
     *
     * @since    1.0.0
     */
    public function __construct() {
        $this->register();
    }

    /**
     * Register all plugin shortcodes
     *
     * @since    1.0.0
     */
    public function register () {
        add_shortcode('rcp-movie-list', array($this, 'movieListShortcode'));
    }

    /**
     * Movie List shortcode
     *
     * @since    1.0.0
     */
    public function movieListShortcode() {
        $recent = new WP_Query();
        $recent->query( 'post_type=rcp_movie&showposts=10' );
        if ($recent->have_posts()) {
            $html = '<ul>';
            while( $recent->have_posts() ) : $recent->the_post();
                $html .='<li>' . get_the_title() . '</li>';
            endwhile;
            $html .= '<ul>';
        } else {
            $html = '<div class="text-danger">There is no Movies Yet!</div>';
        }
        return $html;
    }
}