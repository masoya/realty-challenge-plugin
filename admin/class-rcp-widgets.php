<?php

/**
 * The widget setup of the plugin.
 *
 * @package    Rcp
 * @subpackage Rcp/admin
 * @author     Noah s <noah.s@realtyna.com>
 */
class Rcp_Widgets extends WP_Widget {

    /**
     * The construct part
     *
     * @since    1.0.0
     */
    function __construct() {
        parent::__construct(

            // Base ID of your widget
            'rcp_movies_count',

            // Widget name will appear in UI
            __('RCP Movies Count', 'realtyna_challenge'),

            // Widget description
            array( 'description' => __( 'This widget show the number of movies.', 'realtyna_challenge' ), )
        );
    }

    /**
     * Creating widget front-end
     *
     * @since    1.0.0
     */
    public function widget( $args, $instance ) {
        $title = apply_filters( 'widget_title', $instance['title'] );

        echo '<div class="rcp-movies-count-widget">';
            echo $args['before_widget'];
            if ( ! empty( $title ) )
                echo $args['before_title'] . $title . $args['after_title'];

            $moviesCount = wp_count_posts('rcp_movie')->publish;

            echo '<span class="count">' . $moviesCount . '</span>';

            echo $args['after_widget'];
        echo '</div>';
    }

    /**
     * Widget Backend
     *
     * @since    1.0.0
     */
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        else {
            $title = __( 'RCP Movies Count', 'realtyna_challenge' );
        }
        // Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <?php
    }

    /**
     * Updating widget replacing old instances with new
     *
     * @since    1.0.0
     */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }
}