<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://realtyna.com/
 * @since      1.0.0
 *
 * @package    Rcp
 * @subpackage Rcp/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Rcp
 * @subpackage Rcp/admin
 * @author     Noah s <noah.s@realtyna.com>
 */
class Rcp_Admin {

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $rcp    The ID of this plugin.
     */
    private $rcp;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * The variable that's responsible for maintaining movie post type instance
     *
     * @since    1.0.0
     * @access   public
     * @var      Rcp_Movie_Post_Type    $moviePostType    Maintains and movie post type instance.
     */
    public $moviePostType;

    /**
     * The variable that's responsible for maintaining shortcodes instance
     *
     * @since    1.0.0
     * @access   public
     * @var      Rcp_Shortcodes    $shortcodes    Maintains and movie post type instance.
     */
    public $shortcodes;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $rcp       The name of this plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct( $rcp, $version ) {

        $this->rcp = $rcp;
        $this->version = $version;

        $this->loadDependencies();
    }

    /**
     * Load the required dependencies for admin area.
     *
     * @since    1.0.0
     * @access   private
     */
    private function loadDependencies() {

        /**
         * The class responsible for registering movie post type,
         * related taxonomies and related custom fields
         */
        require_once RCP_PATH . 'admin/class-rcp-movie-post-type.php';
        require_once RCP_PATH . 'admin/class-rcp-shortcodes.php';
        require_once RCP_PATH . 'admin/class-rcp-widgets.php';

        $this->moviePostType = new Rcp_Movie_Post_Type();
        $this->shortcodes = new Rcp_Shortcodes();
    }

    /**
     * Register the stylesheets for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueueStyles() {

        /**
         * An instance of this class should be passed to the run() function
         * defined in Rcp_Loader as all the hooks are defined
         * in that particular class.
         *
         * The Rcp_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        $screen = get_current_screen();
        if ($screen->base == 'post' && $screen->post_type == 'rcp_movie') {
            wp_enqueue_style($this->rcp . '-movies-stylesheet', RCP_URL . 'admin/assets/css/movie-styles.css', array(), $this->version, 'all');
        }
    }

    /**
     * Register the JavaScript for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueueScripts() {

        /**
         * An instance of this class should be passed to the run() function
         * defined in Rcp_Loader as all the hooks are defined
         * in that particular class.
         *
         * The Rcp_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_script($this->rcp . '-admin', RCP_URL . 'admin/assets/js/rcp-admin.js', array( 'jquery' ), $this->version, false);
    }

    /**
     * Register Widgets
     *
     * @since    1.0.0
     */
    public function registerWidgets() {
        register_widget( 'Rcp_Widgets' );
    }
}