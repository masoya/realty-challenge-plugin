<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://realtyna.com/
 * @since             1.0.0
 * @package           Rcp
 *
 * @wordpress-plugin
 * Plugin Name:       Realtyna Challenge Plugin
 * Plugin URI:        https://realtyna.com/
 * Description:       This is Realtyna Challenge Plugin (RCP)!
 * Version:           1.0.0
 * Author:            Noah s
 * Author URI:        https://realtyna.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       rcp
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */

const RCP_VERSION = '1.0.0';

// Define Plugin path.
define('RCP_PATH', plugin_dir_path( __FILE__ ));

// Define Plugin url.
define('RCP_URL', plugin_dir_url( __FILE__ ));

// Check if Plugin class exists
if ( ! class_exists( 'Realtyna_Challenge_Plugin' ) ) {

    // Require main plugin class
    require RCP_PATH . 'includes/class-rcp.php';

    class Realtyna_Challenge_Plugin {

        /**
         * Constructor
         *
         * Setup and start plugin main functionalities
         *
         * @since    1.0.0
         */
        public function __construct() {
            $this->setup();
        }

        /**
         * Setting up Hooks
         *
         * @since    1.0.0
         * @access   private
         */
        private function setup() {
            //Main plugin hooks
            register_activation_hook( __FILE__, array( $this, 'activate' ) );
            register_deactivation_hook( __FILE__, array( $this, 'deactivate' ) );
        }

        /**
         * Activation plugin hook
         *
         * The code that runs during plugin activation.
         * This action is documented in includes/class-rcp-install.php
         *
         * @since    1.0.0
         */
        public static function activate() {
            require_once RCP_PATH . 'includes/class-rcp-install.php';
            $installer = new Rcp_Install();
            $installer->run();
        }

        /**
         * Deactivation plugin hook
         *
         * The code that runs during plugin deactivation.
         * This action is documented in includes/class-rcp-uninstall.php
         *
         * @since    1.0.0
         */
        public static function deactivate() {
            require_once RCP_PATH . 'includes/class-rcp-uninstall.php';
            $uninstaller = new Rcp_Uninstall();
            $uninstaller->run();
        }

        /**
         * Begins execution of the plugin.
         *
         * Since everything within the plugin is registered via hooks,
         * then kicking off the plugin from this point in the file does
         * not affect the page life cycle.
         *
         * @since    1.0.0
         */
        public function load() {
            $plugin = new Rcp();
            $plugin->run();
        }
    }


    // Initiate the plugin class and begins execution of the plugin.
    $rcpPlugin = new Realtyna_Challenge_Plugin();
    $rcpPlugin->load();
}



