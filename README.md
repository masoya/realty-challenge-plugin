# Realty Challenge Plugin



## Getting started

For view different parts of plugin, please go to "/rcp" path after activate plugin.

For example: http://localhost/wordpress/rcp/

This challenge has Movies (Post Type) entries with Genre taxonomy (for category) and Cast taxonomy (for tags).


Each movie has the following fields: 

- Title

- Content

- image

- Movie Duration

- Director

- IMDB Rating

- Release Date
